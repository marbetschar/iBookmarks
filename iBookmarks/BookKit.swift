//
//  BookKit.swift
//  iBookmarks
//
//  Created by Marco Betschart on 05.04.17.
//  Copyright © 2017 MANDELKIND. All rights reserved.
//

import Foundation


protocol BKDataSource{
	associatedtype Library
	associatedtype Book
	associatedtype Annotation
	
	func libraries() -> [Library]
	func books() -> [Book]
	
	func libraries(for book: Book) -> [Library]
	func books(for library: Library) -> [Book]
	func annotations(for book: Book) -> [Annotation]
}


enum BKBookFormat: String{
	case epub = "epub"
	case pdf = "pdf"
}


protocol BKManager{
	associatedtype Book
	
	func fileExists(_ book: Book) -> Bool
	func fileEncrypted(_ book: Book) -> Bool
	func fileBundled(_ book: Book) -> Bool
	func fileBundle(_ book: Book, to directory: URL) -> Book?
	func fileUnbundle(_ book: Book, to directory: URL) -> Book?
	func fileConvert(_ book: Book, in format: BKBookFormat, to directory: URL) -> Book?
}


protocol BKBook{
	associatedtype DataSource
	associatedtype Library
	associatedtype Annotation
	
	var dataSource: DataSource{ get }
	var id: String?{ get set }
	var format: BKBookFormat? { get }
	
	var annotations: [Annotation]{ get }
	var libraries: [Library]{ get }
}


protocol BKLibrary{
	associatedtype DataSource
	associatedtype Book
	
	var dataSource: DataSource{ get }
	var id: String?{ get set }
	
	var books: [Book]{ get }
}


protocol BKAnnotation{
	associatedtype DataSource
	
	var dataSource: DataSource{ get }
	var id: String?{ get set }
}
