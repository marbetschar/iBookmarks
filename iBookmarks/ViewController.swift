//
//  ViewController.swift
//  iBookmarks
//
//  Created by Marco Betschart on 06.04.17.
//  Copyright © 2017 MANDELKIND. All rights reserved.
//

import Cocoa
import Quartz


class ViewController: NSViewController,NSTableViewDataSource,NSTableViewDelegate{

	
	@IBOutlet weak var tableView: NSTableView!
	var books: [iBook] = []
	
	
	override func viewDidLoad(){
		iBookDataSource.standard.ensureDatabaseAccess{ [weak self] in
            DispatchQueue.main.async { [weak self] in
                guard let strongSelf = self else { return }
                
                strongSelf.books = iBookDataSource.standard.books().filter({ (book) -> Bool in
                    return book.format == .epub && iBookManager.standard.fileExists(book) && !iBookManager.standard.fileEncrypted(book)
                }).sorted(by: { (a, b) -> Bool in
                    return a.title?.trimmingCharacters(in: .whitespacesAndNewlines).lowercased() ?? "" < b.title?.trimmingCharacters(in: .whitespacesAndNewlines).lowercased() ?? ""
                })
                
                strongSelf.tableView.dataSource = self
                strongSelf.tableView.delegate = self
                
                //column 100% width
                strongSelf.tableView.columnAutoresizingStyle = .uniformColumnAutoresizingStyle
                if let tableColumn = strongSelf.tableView.tableColumns.first{
                    tableColumn.resizingMask = .autoresizingMask
                }
                strongSelf.tableView.sizeLastColumnToFit()
            }
		}
	}
	
	
	@IBOutlet weak var annotationsEnabled: NSButton!
	
	@IBAction func saveAsPDF(_ sender: Any) {
		iBookDataSource.standard.ensureDocumentsAccess{ [weak self] in
            DispatchQueue.main.async { [weak self] in
                guard let strongSelf = self else { return }
                let selectedRowIndexes = strongSelf.tableView.selectedRowIndexes
                let annotationsEnabledState = strongSelf.annotationsEnabled.state
                
                DispatchQueue.global(qos: .background).async{ [weak self] in
                    guard let strongSelf = self else { return }
                    
                    let fileManager = FileManager.default
                    let targetURL = URL(fileURLWithPath: NSString(string: "~/Downloads/").expandingTildeInPath)
                    let temporaryURL = fileManager.temporaryDirectory
                    
                    for rowIndex in selectedRowIndexes{
                        let book = strongSelf.books[rowIndex]
                        
                        guard let bookPDF = iBookManager.standard.fileConvert(book, in: .pdf, to: temporaryURL), let bookPDFURL = bookPDF.fileURL else{ continue }
                        let annotations = book.annotations.filter{
                            false == ($0.isDeleted ?? false)
                        }
                        
                        let finalize = { (from: URL, to: URL) -> Void in
                            let fileManager = FileManager.default
                            
                            if fileManager.fileExists(atPath: to.path){
                                do{
                                    try fileManager.removeItem(at: to)
                                } catch let error{
                                    print(error)
                                }
                            }
                            
                            do{
                                try fileManager.moveItem(at: from, to: to)
                            } catch let error{
                                print(error)
                            }
                        }
                        
                        guard annotationsEnabledState == 1 && annotations.count > 0, let docPDF = PDFDocument(url: bookPDFURL) else{
                            finalize(bookPDFURL, targetURL.appendingPathComponent(bookPDFURL.lastPathComponent))
                            continue
                        }
                        
                        for annotation in annotations{
                            guard let selectedText = annotation.selectedText else { continue }
                            var textSelections = [PDFSelection]()
                            
                            //try to get the best match by using the surrounding representativeText
                            if let representativeText = annotation.representativeText{
                                let representativeSelections = docPDF.search(ignoreWhitespacesAndNewlinesFor: representativeText, with: .diacriticInsensitive)
                                
                                for representativeSelection in representativeSelections{
                                    //extend representativeSelection to improve search results
                                    //                            representativeSelection.extend(atStart: 25)
                                    //                            representativeSelection.extend(atEnd: 25)
                                    
                                    guard let textSelection = docPDF.search(ignoreWhitespacesAndNewlinesFor: selectedText, from: representativeSelection, with: .backwards)
                                        ?? docPDF.search(ignoreWhitespacesAndNewlinesFor: selectedText, from: representativeSelection, with: .diacriticInsensitive) else { continue }
                                    textSelections.append(textSelection)
                                }
                            }
                            
                            //if we do not have anything yet, just get the first match
                            if textSelections.isEmpty, let textSelection = docPDF.search(ignoreWhitespacesAndNewlinesFor: selectedText, with: .diacriticInsensitive).first{
                                textSelections = [textSelection]
                            }
                            
                            //still nothing? report it!
                            if textSelections.isEmpty{
                                print("NotFound:",bookPDF.title ?? "","-",selectedText)
                                continue
                            }
                            
                            //merge all selections into one to avoid any overlays/duplicates
                            let textSelection = PDFSelection(document: docPDF)
                            textSelection.add(textSelections)
                            let lines = textSelection.selectionsByLine()
                            
                            for line in lines{
                                for pagePDF in line.pages{
                                    let group = annotation.group ?? iBookAnnotation.Group.markup
                                    let lineBounds = line.bounds(for: pagePDF)
                                    let color = annotation.style?.color ?? iBookAnnotation.Style.yellow.color
                                    
                                    if group == nil{
                                        print("Unknown annotation group: \(annotation.group)")
                                        
                                    } else if group == .markup {
                                        let annotationPDF = PDFAnnotationMarkup(bounds: lineBounds)
                                        annotationPDF.page = pagePDF
                                        
                                        annotationPDF.setMarkupType((annotation.isUnderline ?? false) == true ? .underline : .highlight)
                                        annotationPDF.color = color
                                        annotationPDF.shouldDisplay = true
                                        
                                        /* we need to fix the bounds here, due to a bug in PDFKit
                                         * see: http://www.cocoabuilder.com/archive/cocoa/178803-pdfmarkupannotations-not-showing-when-drawing-pdfpage.html
                                         */
                                        annotationPDF.setQuadrilateralPoints([NSMakeSize(0.0,lineBounds.height),NSMakeSize(lineBounds.width,lineBounds.height),NSMakeSize(0.0,0.0),NSMakeSize(lineBounds.width,0.0)])
                                        
                                        pagePDF.addAnnotation(annotationPDF)
                                        pagePDF.displaysAnnotations = true
                                    }
                                    
                                    guard let additionalText = annotation.note, line == lines.first, pagePDF == line.pages.first else{ continue }
                                    
                                    let annotationPDF = PDFAnnotationText(bounds: lineBounds)
                                    annotationPDF.setIconType(.comment)
                                    annotationPDF.color = color
                                    annotationPDF.contents = additionalText
                                    
                                    pagePDF.addAnnotation(annotationPDF)
                                }
                            }
                        }
                        docPDF.write(to: bookPDFURL)
                        
                        finalize(bookPDFURL, targetURL.appendingPathComponent(bookPDFURL.lastPathComponent))
                    }
                }
            }
		}
	}


	func numberOfRows(in tableView: NSTableView) -> Int{
		return self.books.count
	}
	
	
	func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
		var view = tableView.make(withIdentifier: "cell", owner: self) as? NSTextField
		
		if view == nil{
			view = NSTextField(frame: NSRect(x: 0, y: 0, width: tableView.frame.width, height: 10))
			view?.identifier = "cell"
			view?.isEditable = false
			view?.isBezeled = false
			view?.drawsBackground = false
		}
		view?.stringValue = self.books[row].title?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
		
		return view
	}
}
