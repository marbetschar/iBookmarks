//
//  PDFDocument.swift
//  iBooks2PDF
//
//  Created by Marco Betschart on 29.03.17.
//  Copyright © 2017 MANDELKIND. All rights reserved.
//

import Foundation
import Quartz


extension PDFDocument{
	
	func search(ignoreWhitespacesAndNewlinesFor rawTerm: String, with options: NSString.CompareOptions) -> [PDFSelection]{
		var selections = [PDFSelection]()
		var lastSelection: PDFSelection?
		
		while let selection = self.search(ignoreWhitespacesAndNewlinesFor: rawTerm, from: lastSelection, with: options){
			selections.append(selection)
			lastSelection = selection
		}
		
		return selections
	}
	
	
	func search(ignoreWhitespacesAndNewlinesFor rawTerm: String, from selection: PDFSelection?, with options: NSString.CompareOptions) -> PDFSelection?{
		guard let rawString = self.string else{ return nil }
		
		let regex: NSRegularExpression
		do{ try regex = NSRegularExpression(pattern:"\\s+", options: []) } catch let error { print(error); return nil }

		var strippedWords = [String]()
		var term = regex.stringByReplacingMatches(in: rawTerm, options: [], range: NSRange(location:0, length: rawTerm.characters.count), withTemplate: " ").trimmingCharacters(in: .whitespacesAndNewlines)
		
		while rawString.range(of: term) == nil{
			guard let word = term.components(separatedBy: .whitespacesAndNewlines).last,
				let wordRange = term.range(of: word, options: .backwards, range: nil, locale: nil) else{ break }
			
			term.removeSubrange(wordRange)
			term = term.trimmingCharacters(in: .whitespacesAndNewlines)
			
			strippedWords.insert(word, at: 0)
		}
		
		guard let termSelection = self.findString(term, from: selection, with: options) else{
			return nil
		}
		if strippedWords.isEmpty{ return termSelection }
		
		//make a nice smooth selection until the end of the note which also includes the from the search stripped words
		for word in strippedWords{
			termSelection.extend(atEnd: word.characters.count + 1)
		}
		
		return termSelection
	}
}
