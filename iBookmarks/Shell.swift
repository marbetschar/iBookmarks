//
//  Shell.swift
//  iBooks2PDF
//
//  Created by Marco Betschart on 27.03.17.
//  Copyright © 2017 MANDELKIND. All rights reserved.
//

import Foundation

struct Shell{
	
	static func exec(_ path: String? = nil, cmd: String, args: [String] = []) -> (Int32,String?){
		let task = Process()
        
		task.launchPath = cmd
		task.arguments = args
		
		if let path = path{
			task.currentDirectoryPath = path
		}

		let pipe = Pipe()
		task.standardOutput = pipe
		task.standardError = pipe
		task.launch()
		
		let data = pipe.fileHandleForReading.readDataToEndOfFile()
		let out = String(data:data, encoding:.utf8)
		task.waitUntilExit()
		
		return (task.terminationStatus,out?.trimmingCharacters(in: .whitespacesAndNewlines))
	}
}
