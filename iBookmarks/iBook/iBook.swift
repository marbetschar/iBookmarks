//
//  iBook.swift
//  iBookmarks
//
//  Created by Marco Betschart on 06.04.17.
//  Copyright © 2017 MANDELKIND. All rights reserved.
//

import Foundation


struct iBook: BKBook{
	typealias DataSource = iBookDataSource
	typealias Library = iBookLibrary
	typealias Annotation = iBookAnnotation
	
	let dataSource: iBookDataSource
	init(dataSource: iBookDataSource){
		self.dataSource = dataSource
	}
	
	
	var id: String?
	var title: String?
	var author: String?
	var description: String?
	var fileURL: URL?
	
	
	var format: BKBookFormat?{
		guard let fileURL = self.fileURL else{ return nil }
		return BKBookFormat(rawValue: fileURL.pathExtension)
	}
	
	
	var annotations: [iBookAnnotation]{
		return self.dataSource.annotations(for: self)
	}
	
	
	var libraries: [iBookLibrary]{
		return self.dataSource.libraries(for: self)
	}
	
	
	func copy() -> iBook{
		var copy = iBook(dataSource: self.dataSource)
		
		copy.id = self.id
		copy.title = self.title
		copy.author = self.author
		copy.description = self.description
		copy.fileURL = self.fileURL
		
		return copy
	}
}
