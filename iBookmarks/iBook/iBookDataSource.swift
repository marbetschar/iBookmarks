//
//  iBookDataSource.swift
//  iBookmarks
//
//  Created by Marco Betschart on 06.04.17.
//  Copyright © 2017 MANDELKIND. All rights reserved.
//

import Foundation
import SQLite
import AppKit


struct iBookDataSource: BKDataSource{
	typealias Library = iBookLibrary
	typealias Book = iBook
	typealias Annotation = iBookAnnotation
	
	static var standard = iBookDataSource()
	
	func libraries() -> [iBookLibrary]{
		return []
	}
	
	
	func books() -> [iBook]{
		do{
			return try self.database(for:.books).prepare(Table("ZBKLIBRARYASSET")).map(toBook)
		} catch let error{
			print(error)
			NSApp.presentError(error)
		}
		
		return []
	}
	
	
	func libraries(for book: iBook) -> [iBookLibrary]{
		guard let id = book.id else{ return [] }
		
		return []
	}
	
	
	func books(for library: iBookLibrary) -> [iBook]{
		guard let id = library.id else{ return [] }
		return []
	}
	
	
	func annotations(for book: iBook) -> [iBookAnnotation]{
		guard let id = book.id else{ return [] }
		
		do{
			return try self.database(for: .annotations).prepare(
				Table("ZAEANNOTATION").filter(
					id == Expression<String?>("ZANNOTATIONASSETID")
						&& 0 == Expression<Int?>("ZANNOTATIONDELETED")
			)).map(toAnnotation)
		} catch let error{
			print(error)
			NSApp.presentError(error)
		}
		
		return []
	}
	
	
	enum Database: String{
		case books = "BKLibrary" //~/Library/Containers/com.apple.iBooksX/Data/Documents/BKLibrary/
		case annotations = "AEAnnotation" //~/Library/Containers/com.apple.iBooksX/Data/Documents/AEAnnotation/
		
		var path: String{
			guard let baseURL = iBookDataSource.standard.databaseBaseURL else{
				return ""
			}
			return baseURL.appendingPathComponent(self.rawValue).path
		}
	}
	
	
	var databaseBaseURL: URL?
	var documentsBaseURL: URL?
	
	mutating func ensureDatabaseAccess(completion: (()->Void)? = nil){
		guard let bookmark = UserDefaults.standard.object(forKey: "LibraryContainersiBooksXDataDocuments") as? Data else{
			return self.requestDatabaseAccess(completion: completion)
		}
		do{
			var isStale = false
			self.databaseBaseURL = try URL(resolvingBookmarkData: bookmark, options: .withSecurityScope, relativeTo: nil, bookmarkDataIsStale: &isStale)
			
			if self.databaseBaseURL == nil || false == self.databaseBaseURL!.startAccessingSecurityScopedResource(){
				self.requestDatabaseAccess(completion: completion)
			} else {
				completion?()
			}
			
		} catch let error{
			print(error)
			NSApp.presentError(error)
		}
	}
	
	
	mutating func requestDatabaseAccess(completion: (()->Void)? = nil){
		let alert = NSAlert()
		alert.messageText = "Grant permission for database"
		alert.informativeText = "Please grant read permissions to the iBooks database directory:\n\n~/Library/Containers/com.apple.iBooksX/Data/Documents"
		alert.alertStyle = .informational
		alert.addButton(withTitle: "Proceed")
		alert.runModal()
		
		let panel = NSOpenPanel()
		panel.directoryURL = URL(fileURLWithPath: NSString(string: "~/Library/Containers/com.apple.iBooksX/Data/Documents/").expandingTildeInPath)
		
		panel.canChooseDirectories = true
		panel.allowsMultipleSelection = false
		panel.canChooseFiles = false
		panel.canCreateDirectories = false
		
		panel.begin{
			guard $0 == NSFileHandlingPanelOKButton else{
				return
			}
			
			for url in panel.urls{
				do{
					let bookmarkData = try url.bookmarkData(options: [.withSecurityScope,.securityScopeAllowOnlyReadAccess], includingResourceValuesForKeys: nil, relativeTo: nil)
					UserDefaults.standard.set(bookmarkData, forKey: "LibraryContainersiBooksXDataDocuments")

					var isStale = false
					iBookDataSource.standard.databaseBaseURL = try URL(resolvingBookmarkData: bookmarkData, options: .withSecurityScope, relativeTo: nil, bookmarkDataIsStale: &isStale)
					
				} catch let error {
					print(error)
					NSApp.presentError(error)
				}
				
				completion?()
			}
		}
	}
	
	
	mutating func ensureDocumentsAccess(completion: (()->Void)? = nil){
		guard let bookmark = UserDefaults.standard.object(forKey: "LibraryMobileDocumentsiBooksDocuments") as? Data else{
			return self.requestDocumentsAccess(completion: completion)
		}
		do{
			var isStale = false
			self.documentsBaseURL = try URL(resolvingBookmarkData: bookmark, options: .withSecurityScope, relativeTo: nil, bookmarkDataIsStale: &isStale)
			
			if self.documentsBaseURL == nil || false == self.documentsBaseURL!.startAccessingSecurityScopedResource(){
				self.requestDocumentsAccess(completion: completion)
			} else {
				completion?()
			}
			
		} catch let error{
			print(error)
			NSApp.presentError(error)
		}
	}
	
	
	mutating func requestDocumentsAccess(completion: (()->Void)? = nil){
		let alert = NSAlert()
		alert.messageText = "Grant permissions for export"
		alert.informativeText = "Please grant read permissions to your iCloud to export iBooks as PDF."
		alert.alertStyle = .informational
		alert.addButton(withTitle: "Proceed")
		alert.runModal()
		
		let panel = NSOpenPanel()
		panel.directoryURL = URL(fileURLWithPath: NSString(string: "~/Library/Mobile Documents/iCloud~com~apple~iBooks/Documents/").expandingTildeInPath)
		
		panel.canChooseDirectories = true
		panel.allowsMultipleSelection = false
		panel.canChooseFiles = false
		panel.canCreateDirectories = false
		
		panel.begin{
			guard $0 == NSFileHandlingPanelOKButton else{
				return
			}
			
			for url in panel.urls{
				do{
					let bookmarkData = try url.bookmarkData(options: [.withSecurityScope,.securityScopeAllowOnlyReadAccess], includingResourceValuesForKeys: nil, relativeTo: nil)
					UserDefaults.standard.set(bookmarkData, forKey: "LibraryMobileDocumentsiBooksDocuments")
					
					var isStale = false
					iBookDataSource.standard.documentsBaseURL = try URL(resolvingBookmarkData: bookmarkData, options: .withSecurityScope, relativeTo: nil, bookmarkDataIsStale: &isStale)
					
				} catch let error {
					print(error)
					NSApp.presentError(error)
				}
				
				completion?()
			}
		}
	}
	
	
	private enum iBookError: Error{
		case databaseNotFound
	}
	
	
	private func database(for database: Database) throws -> Connection{
		let path = database.path
		
		guard let url: URL = {
			let enumerator = FileManager.default.enumerator(atPath: path)
			while let fileName = enumerator?.nextObject() as? String{
				if fileName.hasSuffix(".sqlite"){
					return URL(fileURLWithPath: path).appendingPathComponent(fileName)
				}
			}
			return nil
			}() else{
				throw iBookError.databaseNotFound
		}
		
		return try Connection(url.path,readonly:true)
	}
	
	
	private func toBook(_ row: Row) throws -> iBook{
		var book = iBook(dataSource: self)
		
		book.id = row[Expression<String?>("ZASSETID")]
		book.title = row[Expression<String?>("ZTITLE")]
		book.author = row[Expression<String?>("ZAUTHOR")]
		book.description = row[Expression<String?>("ZBOOKDESCRIPTION")]
		
		if let filePath = row[Expression<String?>("ZPATH")]{
			book.fileURL = URL(fileURLWithPath: filePath)
		}
		
		return book
	}
	
	
	private func toAnnotation(_ row: Row) throws -> iBookAnnotation{
		var annotation = iBookAnnotation(dataSource: self)
		
		annotation.id = row[Expression<String?>("Z_PK")]
		annotation.isDeleted = row[Expression<Int?>("ZANNOTATIONDELETED")] == 1 ? true : false
		annotation.isUnderline = row[Expression<Int?>("ZANNOTATIONISUNDERLINE")] == 1 ? true : false
		annotation.representativeText = row[Expression<String?>("ZANNOTATIONREPRESENTATIVETEXT")]
		annotation.selectedText = row[Expression<String?>("ZANNOTATIONSELECTEDTEXT")]
		annotation.note = row[Expression<String?>("ZANNOTATIONNOTE")]
		annotation.group = iBookAnnotation.Group(rawValue: row[Expression<Int?>("ZANNOTATIONTYPE")] ?? -1)
		annotation.style = iBookAnnotation.Style(rawValue: row[Expression<Int?>("ZANNOTATIONSTYLE")] ?? -1)
		
		return annotation
	}
}
