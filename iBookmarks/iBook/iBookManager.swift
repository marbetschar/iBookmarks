//
//  iBookManager.swift
//  iBookmarks
//
//  Created by Marco Betschart on 06.04.17.
//  Copyright © 2017 MANDELKIND. All rights reserved.
//

import Foundation
import ZipArchive
import Quartz


struct iBookManager: BKManager{
	typealias Book = iBook
	
	static let standard = iBookManager()
	
	
	func fileExists(_ book: iBook) -> Bool{
		guard let fileURL = book.fileURL else{ return false }
		return FileManager.default.fileExists(atPath: fileURL.path)
	}
	
	
	func fileEncrypted(_ book: iBook) -> Bool{
		guard let format = book.format, format == .epub else{ return false } //TODO: encryption check for PDF
		
		let bookIsBundled = self.fileBundled(book)
		var unbundle: iBook? = book
		let fileManager = FileManager.default
		
		if bookIsBundled{
			unbundle = self.fileUnbundle(book, to: fileManager.temporaryDirectory)
		}
		guard let bookUnbundled = unbundle, let fileURL = bookUnbundled.fileURL else{ return false }
		let isEncrypted = fileManager.fileExists(atPath: fileURL.appendingPathComponent("META-INF").appendingPathComponent("encryption").appendingPathExtension("xml").path)
		
		if bookIsBundled{
			do{
				try fileManager.removeItem(at: fileURL)
				
			} catch let error{
				print(error)
			}
		}
		
		return isEncrypted
	}
	
	
	func fileBundled(_ book: iBook) -> Bool{
		guard let fileURL = book.fileURL else{ return false }
		
		var isDirectory: ObjCBool = false
		let fileExists = FileManager.default.fileExists(atPath: fileURL.path, isDirectory:&isDirectory)
		
		return fileExists && false == isDirectory.boolValue
	}
	
	
	func fileBundle(_ book: iBook, to directory: URL) -> iBook?{
		guard let fileURL = book.fileURL else{ return nil }
		let bookBundledURL = directory.appendingPathComponent(fileURL.lastPathComponent)
		
		do{
			try SSZipArchive.createZipFile(atPath: bookBundledURL.path, withContentsOfDirectory: fileURL.path)
			
			var bookBundled = book.copy()
			bookBundled.fileURL = bookBundledURL
			return bookBundled
			
		} catch let error{
			print(error)
		}
		
		return nil
	}
	
	
	func fileUnbundle(_ book: iBook, to directory: URL) -> iBook?{
		guard let fileURL = book.fileURL else{ return nil }
		let bookUnbundledURL = directory.appendingPathComponent(fileURL.lastPathComponent)
		
		do{
			try SSZipArchive.unzipFile(atPath: fileURL.path, toDestination: bookUnbundledURL.path)
			
			var bookUnbundled = book.copy()
			bookUnbundled.fileURL = bookUnbundledURL
			
			return bookUnbundled
			
		} catch let error{
			print(error)
		}
		
		return nil
	}
	
	
	func fileConvert(_ book: iBook, in format: BKBookFormat, to directory: URL) -> iBook?{
		guard let from = book.format, from == .epub && format == .pdf else{ return nil } //currently conversion from *.epub to *.pdf
		guard self.fileExists(book) && !self.fileEncrypted(book) else{ return nil }
		
		let fileManager = FileManager.default
		let bookIsBundled = self.fileBundled(book)
		var bundle: iBook? = book
		
		if !bookIsBundled{
			bundle = self.fileBundle(book, to: fileManager.temporaryDirectory)
		}
		guard let bookBundled = bundle, let bookBundledURL = bookBundled.fileURL else{ return nil }
		let bookConvertedURL = directory.appendingPathComponent(bookBundledURL.lastPathComponent).deletingPathExtension().appendingPathExtension(format.rawValue.lowercased())
		
		let ebookConvert = "/Applications/calibre.app/Contents/MacOS/ebook-convert"
		guard fileManager.fileExists(atPath: ebookConvert) else{
            print("Calibre app is not installed. Please download and install from: https://calibre-ebook.com")
			return nil
		}
		
		let (convertStatus,convertOutput) = Shell.exec(
			bookBundledURL.deletingLastPathComponent().path,
			cmd:ebookConvert,args:[
				bookBundledURL.lastPathComponent,
				bookConvertedURL.path,
				"--paper-size=a4",
				"--preserve-cover-aspect-ratio",
				"--smarten-punctuation",
				"--pretty-print",
				"--embed-all-fonts",
				"--subset-embedded-fonts",
				"--margin-left=54",
				"--margin-top=54",
				"--margin-right=54",
				"--margin-bottom=54"
			])
		
		if !bookIsBundled{
			do{ try fileManager.removeItem(at: bookBundledURL) } catch _{}
		}
		
		guard convertStatus == 0 else{
			if let output = convertOutput{ print("convertStatus(\(convertStatus)):",output) }
			return nil
		}
		
		/** Workaround for "failed to parse embedded CMap":
		*
		* Mac has no support for unicode cmaps, a feature that was introduced to PDF over six years ago.
		* So we need to run a Quartz Filter on the PDF after a compatible font is embedded during the Calibre conversion process it will make the content of the PDFs searchable and copyable.
		*
		* see: https://www.mobileread.com/forums/showthread.php?t=220576
		*/
		if let docPDF = PDFDocument(url: bookConvertedURL){
			var attributes = docPDF.documentAttributes ?? [:]
            attributes[PDFDocumentAttribute.titleAttribute] = book.title
            attributes[PDFDocumentAttribute.authorAttribute] = book.author
            attributes[PDFDocumentAttribute.subjectAttribute] = book.description
			docPDF.documentAttributes = attributes
			
			docPDF.write(to: bookConvertedURL)
		}
		
		var bookConverted = book.copy()
		bookConverted.fileURL = bookConvertedURL
		
		return bookConverted
	}
}
