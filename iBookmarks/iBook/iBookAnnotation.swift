//
//  iBookAnnotation.swift
//  iBookmarks
//
//  Created by Marco Betschart on 06.04.17.
//  Copyright © 2017 MANDELKIND. All rights reserved.
//

import Foundation
import Cocoa


struct iBookAnnotation: BKAnnotation{
	typealias DataSource = iBookDataSource
	
	let dataSource: iBookDataSource
	init(dataSource: iBookDataSource){
		self.dataSource = dataSource
	}
	
	var id: String?
	var style: iBookAnnotation.Style?
	var group: iBookAnnotation.Group?
	
	var isDeleted: Bool?
	var isUnderline: Bool?
	var representativeText: String?
	var selectedText: String?
	var note: String?
	
	
	enum Group: Int{
		case markup = 2
	}
	
	
	enum Style: Int{
		case red = 0
		case green = 1
		case blue = 2
		case yellow = 3
		case pink = 4
		case violet = 5
		
		var color: NSColor{
			switch(self){
			case .red:
				return NSColor(red:229.0/255.0, green:12.0/255.0, blue:18.0/255.0, alpha: 1.0)
			case .green:
				return NSColor(red:178.0/255.0, green:233.0/255.0, blue:94.0/255.0, alpha: 1.0)
			case .blue:
				return NSColor(red:156.0/255.0, green:203.0/255.0, blue:251.0/255.0, alpha: 1.0)
			case .yellow:
				return NSColor(red:250.0/255.0, green:230.0/255.0, blue:88.0/255.0, alpha: 1.0)
			case .pink:
				return NSColor(red:249.0/255.0, green:155.0/255.0, blue:188.0/255.0, alpha: 1.0)
			case .violet:
				return NSColor(red:203.0/255.0, green:156.0/255.0, blue:251.0/255.0, alpha: 1.0)
			}
		}
	}
}
