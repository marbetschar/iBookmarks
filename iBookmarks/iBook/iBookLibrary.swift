//
//  iBookLibrary.swift
//  iBookmarks
//
//  Created by Marco Betschart on 06.04.17.
//  Copyright © 2017 MANDELKIND. All rights reserved.
//

import Foundation


struct iBookLibrary: BKLibrary{
	typealias DataSource = iBookDataSource
	typealias Book = iBook
	
	let dataSource: iBookDataSource
	init(dataSource: iBookDataSource){
		self.dataSource = dataSource
	}
	
	var id: String?
	
	var books: [iBook]{
		return self.dataSource.books(for: self)
	}
}
