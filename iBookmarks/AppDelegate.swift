//
//  AppDelegate.swift
//  iBookmarks
//
//  Created by Marco Betschart on 06.04.17.
//  Copyright © 2017 MANDELKIND. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

	func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool{
		return true
	}
}
