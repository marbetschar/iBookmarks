class ShareExtension{
	
	run(args){
		var meta = {
			titles: document.querySelectorAll('meta[name="title"]'),
			authors: document.querySelectorAll('meta[property="author"]'),
			descriptions: document.querySelectorAll('meta[name="description"]'),
			og: {
				titles: document.querySelectorAll('meta[property="og:title"]'),
				authors: document.querySelectorAll('meta[property="og:author"]'),
				descriptions: document.querySelectorAll('meta[property="og:description"]')
			}
		};

		var title = document.title;
		if( meta.og.titles.length && meta.og.titles[0].getAttribute('content') && meta.og.titles[0].getAttribute('content').length ){
			title = meta.og.titles[0].getAttribute('content');
		} else if( meta.titles.length && meta.titles[0].getAttribute('content') && meta.titles[0].getAttribute('content').length ){
			title = meta.titles[0].getAttribute('content');
		}
		
		var author = '';
		if( meta.og.authors.length && meta.og.authors[0].getAttribute('content') && meta.og.authors[0].getAttribute('content').length ){
			author = meta.og.authors[0].getAttribute('content');
		} else if( meta.authors.length && meta.authors[0].getAttribute('content') && meta.authors[0].getAttribute('content').length ){
			author = meta.authors[0].getAttribute('content');
		}
		
		var description = '';
		if( meta.og.descriptions.length && meta.og.descriptions[0].getAttribute('content') && meta.og.descriptions[0].getAttribute('content').length ){
			description = meta.og.descriptions[0].getAttribute('content');
		} else if( meta.descriptions.length && meta.descriptions[0].getAttribute('content') && meta.descriptions[0].getAttribute('content').length ){
			description = meta.descriptions[0].getAttribute('content');
		}
		
		args.completionFunction({
			'title': title,
			'author': author,
			'description': description,
			'url': document.location.href
		});
	}
};

//The JavaScript file must contain a global object named "ExtensionPreprocessingJS".
var ExtensionPreprocessingJS = new ShareExtension;
