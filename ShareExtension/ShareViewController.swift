//
//  ShareViewController.swift
//  ShareExtension
//
//  Created by Marco Betschart on 06.04.17.
//  Copyright © 2017 MANDELKIND. All rights reserved.
//

import Foundation
import Cocoa
import Alamofire
import ZipArchive


class ShareViewController: NSViewController {

    override var nibName: String? {
        return "ShareViewController"
    }
	
	@IBOutlet weak var titleField: NSTextField!
	@IBOutlet var descriptionField: TextViewBookDescription!
	
	var activeTab = [String:String]()

    override func loadView() {
        super.loadView()

		if let items = self.extensionContext?.inputItems as? [NSExtensionItem]{
			for item in items{
				guard let attachments = item.attachments else{ continue }
				
				for attachment in attachments{
					guard let provider = attachment as? NSItemProvider, provider.hasItemConformingToTypeIdentifier(kUTTypePropertyList as String) else{ continue }
					
					provider.loadItem(forTypeIdentifier: kUTTypePropertyList as String, options: nil){ (result, error) in
						guard error == nil else{ print(error); return }
						guard let properties = result as? NSDictionary, let activeTab = properties[NSExtensionJavaScriptPreprocessingResultsKey] as? [String:String] else{ return }
						
						self.activeTab = activeTab
						self.titleField.stringValue = self.activeTab["title"] ?? ""
						self.descriptionField.string = self.activeTab["description"]
					}
				}
			}
		}
    }

	
	@IBAction func download(_ sender: Any){
		self.extensionContext?.completeRequest(returningItems: nil, completionHandler: nil)
		
		let dispatchQueue = DispatchQueue.global(qos: .background)
		dispatchQueue.async{
			let parameters: Parameters = [
				"title": self.titleField.stringValue
				,"author": self.activeTab["author"]
				,"description": self.descriptionField.string
				,"urls": [self.activeTab["url"]]
			]
			
			Alamofire.request("https://epub.press/api/v1/books", method: .post, parameters: parameters, encoding: JSONEncoding.default)
				.validate().responseJSON(queue: dispatchQueue){ response in
					guard response.result.isSuccess else{
						if let error = response.result.error{ print(error) }
						return
					}
					guard let json = response.result.value as? [String:Any], let bookId = json["id"] as? String else{
						return
					}
					
					var requestIsRunning = false
					var bookIsFinished = false
					var requestFailures = 0
					
					repeat{
						while requestIsRunning{
							sleep(1)
						}
						
						requestIsRunning = true
						Alamofire.request("https://epub.press/api/v1/books/\(bookId)/status", method: .get)
							.validate().responseJSON(queue: dispatchQueue){ response in
							requestIsRunning = false
							
							guard response.result.isSuccess, let json = response.result.value as? [String:Any], let progress = json["progress"] as? Int else{
								requestFailures += 1
								if let error = response.result.error{ print(error) }
								return
							}
							
							if progress >= 100{
								bookIsFinished = true
							}
						}
					} while bookIsFinished == false && requestFailures < 3
					
					Alamofire.request("https://epub.press/api/v1/books/\(bookId)/download", method: .get)
						.validate().responseData(queue: dispatchQueue){ response in
							guard let data = response.data else{
								return
							}
							
							let fileManager = FileManager.default
							let fileNamePathSafe = self.titleField.stringValue.components(separatedBy: CharacterSet(charactersIn: "\"\\/?<>:*|")).joined(separator: " ")
							let fileURL = fileManager.temporaryDirectory.appendingPathComponent(fileNamePathSafe).appendingPathExtension("epub")
							
							if fileManager.fileExists(atPath: fileURL.path){
								do{ try fileManager.removeItem(at: fileURL) } catch _ {}
							}
							
							do{ try data.write(to: fileURL) } catch let error{ print(error) }
							guard fileManager.fileExists(atPath: fileURL.path) else{ return }
							
							do{
								let directoryURL = fileURL.deletingPathExtension()
								let opfURL = directoryURL.appendingPathComponent("OEBPF").appendingPathComponent("ebook").appendingPathExtension("opf")

								try SSZipArchive.unzipFile(atPath: fileURL.path, toDestination: directoryURL.path)
								
								if fileManager.fileExists(atPath: opfURL.path){
									let opf = try XMLDocument(contentsOf: opfURL, options: 0)
									
									let removeFromOPF = { (parentName: String, elementName: String, attributeName: String, attributeValue: String) -> Void in
										for parentElement in opf.rootElement()?.elements(forName: parentName) ?? []{
											for (i,element) in parentElement.elements(forName: elementName).enumerated(){
												guard let elementAttributeValue = element.attribute(forName: attributeName)?.stringValue else{ continue }
												if attributeValue == elementAttributeValue{
													parentElement.removeChild(at: i)
													return
												}
											}
										}
									}
									
									removeFromOPF("manifest", "item", "id", "cover-image")
									removeFromOPF("manifest", "item", "id", "cover")
									removeFromOPF("manifest", "item", "id", "toc")
									
									removeFromOPF("spine", "itemref", "idref", "cover")
									removeFromOPF("spine", "itemref", "idref", "toc")
									
									removeFromOPF("guide", "reference", "type", "toc")
									
									try opf.xmlString.write(to: opfURL, atomically: false, encoding: .utf8)
									
									/** remove the cover for now to make the book distinguishable - see https://github.com/haroldtreen/epub-press-clients/issues/9 */
									let coverURL = directoryURL.appendingPathComponent("OEBPF").appendingPathComponent("cover").appendingPathExtension("xhtml")
									
									if fileManager.fileExists(atPath: coverURL.path){
										do{ try fileManager.removeItem(at: coverURL) } catch _ {}
									}
									
									/** remove the cover image for now to make the book distinguishable - see https://github.com/haroldtreen/epub-press-clients/issues/9 */
									let coverImageURL = directoryURL.appendingPathComponent("OEBPF").appendingPathComponent("images").appendingPathComponent("cover").appendingPathExtension("png")
									if fileManager.fileExists(atPath: coverImageURL.path){
										do{ try fileManager.removeItem(at: coverImageURL) } catch _ {}
									}
									
									/** since we are only printing one article right now, remove the table of contents - see https://github.com/haroldtreen/epub-press-clients/issues/13 */
									let tocURL = directoryURL.appendingPathComponent("OEBPF").appendingPathComponent("content").appendingPathComponent("toc").appendingPathExtension("xhtml")
									if fileManager.fileExists(atPath: tocURL.path){
										do{ try fileManager.removeItem(at: tocURL) } catch _ {}
									}
									
									/** repackage the book */
									if fileManager.fileExists(atPath: fileURL.path){
										do{ try fileManager.removeItem(at: fileURL) } catch _ {}
									}
									
									try SSZipArchive.createZipFile(atPath: fileURL.path, withContentsOfDirectory: directoryURL.path)
								}
								
								if fileManager.fileExists(atPath: directoryURL.path){
									try fileManager.removeItem(at: directoryURL)
								}
								
							} catch let error{
								print(error)
							}
							
							if fileManager.fileExists(atPath: fileURL.path){
								let downloadsURL = URL(fileURLWithPath: NSString(string: "~/Downloads/").expandingTildeInPath).appendingPathComponent(fileURL.lastPathComponent)
								
								if fileManager.fileExists(atPath: downloadsURL.path){
									do{ try fileManager.removeItem(at: downloadsURL) } catch _ {}
								}
								
								do{
									try fileManager.moveItem(at: fileURL, to: downloadsURL)
								} catch let error {
									print(error)
								}
							}
					}
			}
		}
	}
	

    @IBAction func cancel(_ sender: AnyObject?) {
        let cancelError = NSError(domain: NSCocoaErrorDomain, code: NSUserCancelledError, userInfo: nil)
        self.extensionContext?.cancelRequest(withError: cancelError)
    }

}
