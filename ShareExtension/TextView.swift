//
//  TextView.swift
//  iBookmarks
//
//  Created by Marco Betschart on 06.04.17.
//  Copyright © 2017 MANDELKIND. All rights reserved.
//

import Foundation
import Cocoa


class TextView: NSTextView{
	var placeholderAttributedString: NSAttributedString?
}


class TextViewBookDescription: TextView{
	override var placeholderAttributedString: NSAttributedString?{
		get{
			return NSAttributedString(string: "Book Description", attributes: [
				NSForegroundColorAttributeName: NSColor(red: 172.0/255.0, green: 172.0/255.0, blue: 172.0/255.0, alpha: 0.9),
				NSFontAttributeName: NSFont.systemFont(ofSize: NSFont.systemFontSize())
			])
		}
		set{
			super.placeholderAttributedString = newValue
		}
	}
}
